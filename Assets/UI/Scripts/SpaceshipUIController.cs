﻿using System;
using System.ComponentModel;
using SpaceshipGen;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpaceshipUIController : MonoBehaviour
{
	public Spaceship spaceship;

	public InputField seedField;
	public Toggle randomSeedToggle;

	public ParameterUIController parameterUI;

	void Start()
	{
		seedField.value = spaceship.seed.ToString();
		randomSeedToggle.isOn = spaceship.RandomSeed;
		spaceship.parameterChanged += (sender, args) => SetValidValues(args.IsGenerated);

		parameterUI.GenerateUI(spaceship.parameters);
	}

	/// <summary>
	/// Reads values from the interface and updates the spaceship with them.
	/// </summary>
	public void UpdateSpaceshipParameters()
	{
		if (spaceship == null)
			return;

		if (!spaceship.RandomSeed)
		{
			var seedText = seedField.value;
			int seed;

			if (int.TryParse(seedText, out seed))
			{
				spaceship.seed = seed;
			}
			else
			{
				spaceship.seed = seedText.GetHashCode();
			}
		}
	}

	/// <summary>
	/// Replaces wrong values on the interface with the ones that will actually be used.
	/// </summary>
	public void SetValidValues(bool resetValues)
	{
		var seedText = seedField.value;
		int seed;

		if (resetValues || int.TryParse(seedText, out seed))
			seedField.value = spaceship.seed.ToString();

		randomSeedToggle.isOn = spaceship.RandomSeed;
	}
}
