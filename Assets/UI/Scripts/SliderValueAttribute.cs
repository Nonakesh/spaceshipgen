﻿[System.AttributeUsage(System.AttributeTargets.Field)]
public class SliderValueAttribute : System.Attribute
{
	public string labelText;
	public float minValue;
	public float maxValue;
	public bool wholeNumbers;

	/// <summary>
	/// The value will always be smaller or equal to this value
	/// </summary>
	public string keepSmallerThan;

	/// <summary>
	/// The value will always be bigger or equal to this value
	/// </summary>
	public string keepBiggerThan;

	public SliderValueAttribute(string labelText, float minValue, float maxValue, bool wholeNumbers, string keepSmallerThan = null,
		string keepBiggerThan = null)
	{
		this.labelText = labelText;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.wholeNumbers = wholeNumbers;
		this.keepSmallerThan = keepSmallerThan;
		this.keepBiggerThan = keepBiggerThan;
	}
}
