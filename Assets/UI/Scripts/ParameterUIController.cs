﻿using System;
using System.Collections;
using System.Reflection;
using SpaceshipGen;
using UnityEngine;
using UnityEngine.UI;

public class ParameterUIController : MonoBehaviour
{
	public ParameterSlider sliderPrefab;

	/// <summary>
	/// The panel that will be the parent of all parameter sliders.
	/// </summary>
	public RectTransform parameterPanel;

	private SpaceshipParameters parameters;

	public void GenerateUI(SpaceshipParameters parameters)
	{
		//Destroys the children of parameter panel, to make place for new parameters.
		if (parameterPanel == null) return;

		foreach (var child in parameterPanel)
		{
			Destroy((child as RectTransform).gameObject);
		}

		this.parameters = parameters;

		//Creates a slider for every parameter in the parameters object.
		var fields = parameters.GetType().GetFields(BindingFlags.GetField | BindingFlags.Public | BindingFlags.Instance);

		foreach (var field in fields)
		{
			var attributes = field.GetCustomAttributes(typeof (SliderValueAttribute), true);

			if (attributes.Length <= 0 || attributes[0] == null)
			{
				throw new ParameterUIGenerationException("The parameter " + field.Name + " does not have the SliderValue attribtue.");
			}

			var s = attributes[0] as SliderValueAttribute;
			var slider = Instantiate(sliderPrefab) as ParameterSlider;
			slider.transform.parent = parameterPanel;

			float value;

			if (field.FieldType == typeof(float))
			{
				value = (float)field.GetValue(parameters);
			}
			else if (field.FieldType == typeof (int))
			{
				value = (int) field.GetValue(parameters);
			}
			else
			{
				throw new ParameterUIGenerationException("The parameter " + field.Name + " is not of the types float or int.");
			}

			slider.InitializeSlider(field.Name, s.labelText, value, s.minValue, s.maxValue, s.wholeNumbers);
			slider.sliderChanged += OnSliderChanged;
		}

		StartCoroutine(SetHeight());
	}

	private IEnumerator SetHeight()
	{
		yield return null;

		float minY = 0;

		foreach (var child in parameterPanel)
		{
			var r = (child as RectTransform);
			float y = r.position.y - r.rect.height;

			if (y < minY)
				minY = y;
		}

		var size = parameterPanel.sizeDelta;
		size.y = parameterPanel.position.y - minY;
		parameterPanel.sizeDelta = size;
	}

	private void OnSliderChanged(object o, SliderChangedHandlerArgs args)
	{
		var value = args.Value;
		bool wholeNumbers = args.Slider.wholeNumbers;

		var field = parameters.GetType().GetField(args.ParameterName);

		//Check "SmallerThan" and "BiggerThan" contraints.
		var s = field.GetCustomAttributes(typeof(SliderValueAttribute), true)[0] as SliderValueAttribute;
		if (s.keepSmallerThan != null)
		{
			var compare = parameters.GetType().GetField(s.keepSmallerThan);
			//This is needed, because Unity or reflection in general is very picky with types.
			var f = wholeNumbers ? (int)compare.GetValue(parameters) : (float)compare.GetValue(parameters);

			if (f < value)
			{
				value = f;
				args.Slider.value = wholeNumbers ? (int)value : value;
			}
		}
		if (s.keepBiggerThan != null)
		{
			var compare = parameters.GetType().GetField(s.keepBiggerThan);
			//This is needed, because Unity or reflection in general is very picky with types.
			var f = wholeNumbers ? (int)compare.GetValue(parameters) : (float)compare.GetValue(parameters);

			if (f > value)
			{
				value = f;
				args.Slider.value = wholeNumbers ? (int)value : value;
			}
		}

		//This is a small hack that has to be used, so that reflection doesn't throw an error.. no idea why!
		object output;
		if (field.FieldType == typeof (float))
		{
			output = value;
		}
		else
		{
			output = (int) value;
		}

		field.SetValue(parameters, output);
	}

	public class ParameterUIGenerationException : Exception
	{
		public ParameterUIGenerationException(string message) : base(message) {}
	}
}
