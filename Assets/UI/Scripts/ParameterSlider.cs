﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ParameterSlider : MonoBehaviour
{
	public Text text;
	public Slider slider;

	public event SliderChangedHandler sliderChanged;

	private string parameterName;

	/// <summary>
	/// Will be called by the slider component. This should not be called manually.
	/// </summary>
	public void OnSliderChanged()
	{
		if (sliderChanged == null)
			return;

		sliderChanged.Invoke(this, new SliderChangedHandlerArgs(slider, parameterName, slider.value));
	}

	/// <summary>
	/// Initializes the slider.
	/// </summary>
	/// <param name="parameterName">The parameter name that will be changed per reflection.</param>
	/// <param name="labelText">The text the slider label will display.</param>
	/// <param name="value">The start value of the slider.</param>
	/// <param name="minValue">The minimal slider value.</param>
	/// <param name="maxValue">The maximal slider value</param>
	/// <param name="wholeNumbers">Should the slider only have whole numbers?</param>
	public void InitializeSlider(string parameterName, string labelText, float value, float minValue, float maxValue, bool wholeNumbers)
	{
		this.parameterName = parameterName;
		text.text = labelText;
		slider.wholeNumbers = wholeNumbers;
		slider.minValue = minValue;
		slider.maxValue = maxValue;
		slider.value = value;
	}
}

public delegate void SliderChangedHandler(object o, SliderChangedHandlerArgs args);

public class SliderChangedHandlerArgs
{
	public string ParameterName { get; private set; }
	public float Value { get; private set; }
	public Slider Slider { get; private set; }

	public SliderChangedHandlerArgs(Slider slider, string parameterName, float value)
	{
		Slider = slider;
		ParameterName = parameterName;
		Value = value;
	}
}
