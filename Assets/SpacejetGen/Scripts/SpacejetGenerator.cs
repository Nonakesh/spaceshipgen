﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace SpaceshipGen
{
	// ==============================================
	// This class, and everything that relates to it
	// was meant as a quick test, and might not be 
	// very well designed... it should work though...
	// ==============================================

	public static class SpacejetGenerator
	{
		public static Transform GenerateSpacejet(int seed, SpacejetPart[] parts, SpacejetParameters parameters)
		{
			Random.seed = seed;

			var root = new GameObject("SpaceJet").transform;

			var shufflebag = new List<SpacejetPart>();

			foreach (var part in parts)
			{
				for (int i = 0; i < part.shufflebagCount; i++)
				{
					shufflebag.Add(part);
				}
			}

			shufflebag = shufflebag.Shuffle().ToList();

			var midPart = shufflebag.First(x => x.isRootPart);
			shufflebag.Remove(midPart);

			midPart = Object.Instantiate(midPart, Vector3.zero, midPart.transform.rotation) as SpacejetPart;
			midPart.transform.parent = root;

			RecursiveAddParts(midPart, root, shufflebag, parameters);

			return root;
		}

		private static void RecursiveAddParts(SpacejetPart part, Transform root, List<SpacejetPart> shufflebag, SpacejetParameters p)
		{
			var t = part.transform;

			//If you don't do this, the results will be predictable because the order is always the same.
			var connections = part.connections.Shuffle();

			foreach (var c in connections)
			{
				if(c.Blocked)
					continue;

				//Find all parts that have a opposing connection point.
				SpacejetPart newPart = null;
				bool mirrored = false;
				foreach (var spacejetPart in shufflebag)
				{
					if (spacejetPart.connections.Any(y => spacejetPart.transform.TransformDirection(y.direction).IsOpposite(t.TransformDirection(c.direction))))
					{
						newPart = spacejetPart;
						shufflebag.Remove(newPart);
						break;
					}

					//Checks if there is a part with the same direction, that will alter be mirrored.
					if (spacejetPart.canBeMirrored &&
					    spacejetPart.connections.Any(
						    y => spacejetPart.transform.TransformDirection(y.direction).IsOpposite(t.TransformDirection(-c.direction))))
					{
						newPart = spacejetPart;
						shufflebag.Remove(newPart);
						mirrored = true;
						break;
					}
				}

				if (newPart == null)
					continue;

				//Find the vector distance the new part should have from the old one.
				var connection = newPart.connections.First(x => x.direction.IsOpposite(c.direction) || (newPart.canBeMirrored && x.direction.IsOpposite(-c.direction))).position;
				connection = newPart.transform.TransformVector(connection);

				newPart = Object.Instantiate(newPart, t.TransformPoint(c.position) - connection, newPart.transform.rotation) as SpacejetPart;
				newPart.transform.parent = root;

				//Block the connection on the newly instantiated part
				var newConn = newPart.connections.First(x => x.direction.IsOpposite(c.direction) || (newPart.canBeMirrored && x.direction.IsOpposite(-c.direction)));
				newConn.Blocked = true;
				c.Blocked = true;

				if (mirrored)
				{
					var scale = GetMirrorScale(newConn.direction);
					newPart.transform.localScale = MultiplyVector3(newPart.transform.localScale, scale);
					newPart.transform.position += (connection - MultiplyVector3(connection, scale)) * 2;
				}

				RecursiveAddParts(newPart, root, shufflebag, p);
			}
		}

		private static bool IsOpposite(this Vector3 vector, Vector3 opposite)
		{
			return (vector.normalized + opposite.normalized).sqrMagnitude < 0.25;
		}

		private static Vector3 GetMirrorScale(Vector3 v)
		{
			v = new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));

			if (v.x > v.y + v.z)
			{
				return new Vector3(-1, 1, 1);
			}

			if (v.y > v.x + v.z)
			{
				return new Vector3(1, -1, 1);
			}

			if (v.z > v.x + v.y)
			{
				return new Vector3(1, 1, -1);
			}

			throw new ArgumentException("direction must be a valid direction vector.");
		}

		private static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
		{
			T[] elements = source.ToArray();
			for (int i = elements.Length - 1; i >= 0; i--)
			{
				// Swap element "i" with a random earlier element it (or itself)
				// ... except we don't really need to swap it fully, as we can
				// return it immediately, and afterwards it's irrelevant.
				int swapIndex = Random.Range(0, i + 1);
				yield return elements[swapIndex];
				elements[swapIndex] = elements[i];
			}
		}

		private static Vector3 MultiplyVector3(Vector3 v1, Vector3 v2)
		{
			return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}
	}
}