﻿using SpaceshipGen;
using UnityEditor;
using UnityEngine;

namespace SpaceshipGen
{
	[CustomEditor(typeof (SpacejetPart))]
	public class JetPartEditor : Editor
	{
		private static bool editRotation;

		void OnSceneGUI()
		{
			var part = target as SpacejetPart;
			var t = part.transform;

			if (part.connections == null || part.connections.Length == 0)
			{
				return;
			}

			foreach (var connection in part.connections)
			{
				if(!connection.showHandles)
					continue;

				if (connection.direction.Equals(Vector3.zero))
					connection.direction = Vector3.forward;

				var pos = t.TransformPoint(connection.position);
				var dir = t.TransformDirection(connection.direction);

				if (editRotation)
				{
					var rot = Handles.RotationHandle(Quaternion.LookRotation(dir), pos);

					connection.direction = t.InverseTransformDirection(rot * Vector3.forward).normalized;
					Handles.Slider(pos, rot * Vector3.forward);
				}
				else
				{
					connection.position = t.InverseTransformPoint(Handles.PositionHandle(pos, Quaternion.identity));
				}
			}

			if (GUI.changed)
				EditorUtility.SetDirty(target);
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (GUILayout.Button(editRotation ? "Edit attachment point position" : "Edit attachment point rotation"))
			{
				editRotation = !editRotation;
			}

			if (GUI.changed)
				EditorUtility.SetDirty(target);
		}
	}
}