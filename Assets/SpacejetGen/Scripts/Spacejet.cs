﻿using SpaceshipGen;
using UnityEngine;
using System.Collections;
public class Spacejet : MonoBehaviour
{
	public bool randomSeed;
	public int seed;

	public SpacejetPart[] parts;
	public SpacejetParameters parameters;

	private Transform spacejet;

	public void Start()
	{
		GenerateSpacejet();
	}

	private void GenerateSpacejet()
	{
		if (randomSeed)
		{
			var r = new System.Random();
			seed = r.Next();
		}

		if (spacejet != null)
		{
			Destroy(spacejet.gameObject);
		}

		spacejet = SpacejetGenerator.GenerateSpacejet(seed, parts, parameters);
	}
}
