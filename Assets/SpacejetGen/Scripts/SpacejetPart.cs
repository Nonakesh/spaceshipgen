﻿using SpaceshipGen;
using UnityEngine;

public class SpacejetPart : MonoBehaviour
{
	public JetConnection[] connections;

	public bool canBeMirrored;

	public bool isRootPart;
	public int shufflebagCount;
}