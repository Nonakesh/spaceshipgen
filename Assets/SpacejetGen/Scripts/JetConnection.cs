﻿using UnityEngine;
using System;

namespace SpaceshipGen
{
	[Serializable]
	public class JetConnection
	{
		public Vector3 position;
		public Vector3 direction;

		public bool Blocked { get; set; }

#if UNITY_EDITOR
		public bool showHandles;
#endif
	} 
}