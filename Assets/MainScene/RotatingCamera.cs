﻿using UnityEngine;
using System.Collections;

public class RotatingCamera : MonoBehaviour
{
	public float rotationSpeed = 25;

	// Update is called once per frame
	void Update ()
	{
		transform.RotateAround(Vector3.zero, Vector3.up, rotationSpeed * Time.deltaTime);
	}
}
