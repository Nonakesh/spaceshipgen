﻿Shader "Lux/Bumped Specular Metalness" {

Properties {
	_Color ("Diffuse Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
	_SpecTex ("Metalness (R) AO (G) Spec (B) Roughness (A)", 2D) = "black" {}
	_BumpMap ("Normalmap", 2D) = "bump" {}
	_EmissionMap("Emission", 2D) = "black" {}
	_EmissionStrength("Emission Strength", Range(-1, 0)) = -1
	_EmissionBoost("Emission Boost", Range(1, 2)) = 1
	_DiffCubeIBL ("Custom Diffuse Cube", Cube) = "black" {}
	_SpecCubeIBL ("Custom Specular Cube", Cube) = "black" {}
	
	_Generate("Generation Percentage", Range(0, 1)) = 1
	_GridColor("Grid Color", Color) = (0.1, 0.1, 1, 1)
	_GridScale("Grid Scale", float) = 2
	_GridThickness("Grid Thickness", float) = 0.2

	_SpaceshipRadius("Spaceship Size", float) = 100

	_HologramSpeedModifier("Hologram Speed Modifier", Range(1, 2)) = 1.2

	_MaterializationColor("Materialization Color", Color) = (1, 0.4, 0.1, 1)

	// _Shininess property is needed by the lightmapper - otherwise it throws errors
	[HideInInspector] _Shininess ("Shininess (only for Lightmapper)", Float) = 0.5
}

SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 400
	
	CGPROGRAM
	#pragma surface surf LuxDirect noambient
	#pragma glsl
	#pragma target 3.0

	// #pragma debug

	#pragma multi_compile LUX_LIGHTING_BP LUX_LIGHTING_CT
	#pragma multi_compile LUX_LINEAR LUX_GAMMA
	#pragma multi_compile DIFFCUBE_ON DIFFCUBE_OFF
	#pragma multi_compile SPECCUBE_ON SPECCUBE_OFF
	// AO is not needed here as it is stored in _SpecTex
	#define LUX_METALNESS


//	#define LUX_LIGHTING_CT
//	#define LUX_LINEAR
//	#define DIFFCUBE_ON
//	#define SPECCUBE_ON
	
	// include should be called after all defines
	#include "LuxCore/LuxLightingDirect.cginc"

	float4 _Color;
	sampler2D _MainTex;
	sampler2D _SpecTex;
	sampler2D _BumpMap;
	sampler2D _EmissionMap;
	float _EmissionStrength;
	float _EmissionBoost;
	#ifdef DIFFCUBE_ON
		samplerCUBE _DiffCubeIBL;
	#endif
	#ifdef SPECCUBE_ON
		samplerCUBE _SpecCubeIBL;
	#endif
	
	// Is set by script
	float4 ExposureIBL;

	struct Input {
		float2 uv_MainTex;
		float2 uv_BumpMap;
		float3 viewDir;
		float3 worldNormal;
		float3 worldRefl;
		float3 worldPos;
		INTERNAL_DATA
	};

	float _Generate;
	float4 _GridColor;
	float _GridScale;
	float _GridThickness;
	float _SpaceshipRadius;
	float _HologramSpeedModifier;

	float4 _MaterializationColor;

	void surf (Input IN, inout SurfaceOutputLux o) {
		fixed4 diff_albedo = tex2D(_MainTex, IN.uv_MainTex);
		// Metal (R) AO (G) Spec (B) Roughness (A)
		fixed4 spec_albedo = tex2D(_SpecTex, IN.uv_MainTex);
	
	//	Diffuse Albedo
		// We have to "darken" diffuse albedo by metalness as it controls ambient diffuse lighting
		o.Albedo = diff_albedo.rgb * _Color.rgb * (1.0 - spec_albedo.r);
		
		o.Alpha = diff_albedo.a * _Color.a;
		o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
	
	//	Specular Color
		// Lerp between specular color (defined as shades of gray for dielectric parts in the blue channel )
		// and the diffuse albedo color based on "Metalness"
		o.SpecularColor = lerp(spec_albedo.bbb, diff_albedo.rgb, spec_albedo.r);
		
		// Roughness – gamma for BlinnPhong / linear for CookTorrence
		o.Specular = LuxAdjustSpecular(spec_albedo.a);
		#if defined(UNITY_PASS_PREPASSFINAL)
			#if defined(SPECCUBE_ON)
				// Fake Fresnel effect using N dot V / only needed by deferred lighting	
				o.DeferredFresnel = exp2(-OneOnLN2_x6 * max(0, dot(o.Normal, normalize(IN.viewDir) )));
			#else
				o.DeferredFresnel = 0.0;
			#endif	
		#endif
		
		#include "LuxCore/LuxLightingAmbient.cginc"

		//Grid Coordinates
		//The +10000 is a hack to prevent visual errors
		float x = IN.worldPos.x + _Time.y + 10000;
		float y = IN.worldPos.y + _Time.y + 10000;
		float z = IN.worldPos.z + _Time.y + 10000;

		float temp =	saturate((fmod(x, _GridScale)/_GridScale)/_GridThickness - 1/_GridThickness + 1);
		float grid = (temp > 0.5 ? 1-temp : temp);

		temp =			saturate((fmod(y, _GridScale)/_GridScale)/_GridThickness - 1/_GridThickness + 1);
		grid += (temp > 0.5 ? 1-temp : temp);

		temp =			saturate((fmod(z, _GridScale)/_GridScale)/_GridThickness - 1/_GridThickness + 1);
		grid += (temp > 0.5 ? 1-temp : temp);

		float generationBorder = (2 * _Generate) * _SpaceshipRadius * _HologramSpeedModifier;

		grid *= (length(IN.worldPos) < generationBorder ? 1 : 0);

		temp = length(IN.worldPos) - generationBorder;
		grid += temp * (temp > 0 && temp < 1.2f ? temp : 0);

		//MaterializationEffect
		float matPercent = saturate(2 * _Generate - 1);
		float matBorder = saturate((-IN.worldPos.z + _SpaceshipRadius * (2 * matPercent - 1)) * 0.5f);

		float materialized = matBorder > 0.01;

		if(grid + matBorder < 0.01)
			discard;
		
		o.Albedo *= materialized;
		o.Specular *= materialized;
		o.SpecularColor *= materialized;
		o.Emission = o.Emission * materialized + grid * _GridColor * 10 * (1 - matBorder) + materialized * 50 * _MaterializationColor * (1 - matBorder);

		//Emission texture
		o.Emission += pow(saturate(tex2D(_EmissionMap, IN.uv_MainTex) + _EmissionStrength) * _EmissionBoost * materialized, 2) * 5;
	}
ENDCG
}
FallBack "Specular"
CustomEditor "LuxMaterialInspector"
}
